# Ready Set Boole
### Exercises about boolean algebra (and more!)

![Screenshot 1](https://gitlab.com/BeBel42/ready-set-boole/-/raw/main/screenshots/1.png)

Ready Set Boole is a project from [campus 19](https://campus19.be/).  
It consists of doing some functions, following [instructions](https://gitlab.com/BeBel42/n-puzzle/-/blob/main/en.subject.pdf?ref_type=heads)
that solves maths problems.
This project uses c++20.

## Technical details
A Makefile is provided.  
`make` to build the project, `make run` to build (if needed) and execute it.

## Goal of project
To learn about boolean algebra, RPN, gray code, set theory and space-filling curves.
